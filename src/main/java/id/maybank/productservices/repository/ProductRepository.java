package id.maybank.productservices.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import id.maybank.productservices.model.Product;

public interface ProductRepository extends MongoRepository<Product, String>{
    
}
