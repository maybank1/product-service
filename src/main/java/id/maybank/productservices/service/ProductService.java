package id.maybank.productservices.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import id.maybank.productservices.model.Product;
import id.maybank.productservices.model.dto.ProductRequest;
import id.maybank.productservices.model.dto.ProductResponse;
import id.maybank.productservices.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    // @Autowired
    // private ProductRepository productRepository;

    private final ProductRepository productRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    // public ProductService(ProductRepository productRepository) {
    //     this.productRepository = productRepository;
    // }
    
    public void createProduct(ProductRequest productRequest) {
        // Product product = Product.builder().name(productRequest.getName())
        // .description(productRequest.getDescription())
        // .price(productRequest.getPrice()).build();
        // product.setName(productRequest.getName());
        // product.setDescription(productRequest.getDescription());
        // product.setPrice(productRequest.getPrice());
        Product product = modelMapper.map(productRequest, Product.class);

        productRepository.save(product);
        log.info("Product {} is saved", product.getName());
    }

    public List<ProductResponse> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(this::mapToProductResponse).toList();
   }

    public ProductResponse getProductById(String id) {
        Optional<Product> product = productRepository.findById(id);
        ProductResponse productResponse = modelMapper.map(product, ProductResponse.class);
        return productResponse;
    }

    private ProductResponse mapToProductResponse(Product product) {
        return modelMapper.map(product, ProductResponse.class);
    }

    public void deleteProduct(String id) {
        productRepository.deleteById(id);
    }

    public ProductResponse updateProduct(ProductResponse productResponse) {
        Product product = productRepository.findById(productResponse.getId()).get();
        product.setName(productResponse.getName());
        product.setDescription(productResponse.getDescription());
        product.setPrice(productResponse.getPrice());
        productRepository.save(product);
        ProductResponse newProduct = modelMapper.map(product, ProductResponse.class);
        return newProduct;
    }

}
