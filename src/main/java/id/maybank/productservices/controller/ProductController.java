package id.maybank.productservices.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.maybank.productservices.model.dto.ProductRequest;
import id.maybank.productservices.model.dto.ProductResponse;
import id.maybank.productservices.service.ProductService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    
    //create product
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public void createProduct(@RequestBody ProductRequest productRequest) {
        productService.createProduct(productRequest);
    }

    //get all product
    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<ProductResponse> getAllProducts(){
        return productService.getAllProducts();

    }

    //get product by id
    @GetMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ProductResponse getProductById(@PathVariable String id) {
        return productService.getProductById(id);
    }

    //delete product
    @GetMapping("/delete/{id}")
    public void deleteProduct(@PathVariable String id) {
        productService.deleteProduct(id);
    }

    //update product
    @PostMapping("/update")
    public ProductResponse updateProduct(@RequestBody ProductResponse productResponse) {
        return productService.updateProduct(productResponse);
    }
}
